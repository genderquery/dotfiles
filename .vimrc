" If plug isn't already installed, fetch and install it
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('$HOME/.vim/plugged')

" A dark colour scheme
Plug 'w0ng/vim-hybrid'

" Full path fuzzy file, buffer, mru, tag, etc finder
Plug 'ctrlpvim/ctrlp.vim'

" Context-sensitive color name highligher
Plug 'ap/vim-css-color'

" For ca65 assembly
Plug 'maxbane/vim-asm_ca65'

call plug#end()

" turn off vi compatibility
set nocompatible

" enable auto filetype detection, indentation, and syntax highlighting
filetype indent plugin on
syntax on

" do not unload buffers when abandoned, just hide them
set hidden

" enable better command completion
set wildmenu
set wildmode=full

" show partial commands
set showcmd

" use case insentive search, except when using capital letters
set ignorecase
set smartcase

" automatically indent to the same level as the previous line
set autoindent

" always show the status line
set laststatus=2

" show dialog instead of just failing certain commands
set confirm

" disable beeps and flashes
set visualbell
set t_vb=

" disable the mouse
set mouse=""

" allow to backspace over everything
set backspace=indent,eol,start

" enable modelines (Debian disables)
set modeline
set modelines=5

" override 'ignorecase' when pattern has upper case characters
set smartcase

" show whitespace characters
set list
set listchars=eol:\ ,tab:→\ ,space:\ ,trail:•,extends:⟩,precedes:⟨,nbsp:␣
set showbreak=↪\

" make it pretty
set background=dark
colorscheme hybrid

" start a war
set shiftwidth=4
set softtabstop=4
set expandtab

" automatically turn on spell check for git commit messages
autocmd BufRead COMMIT_EDITMSG setlocal spell 
